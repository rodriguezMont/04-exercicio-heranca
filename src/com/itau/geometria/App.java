package com.itau.geometria;

import java.util.List;

import com.itau.geometria.formas.FabricaFormas;
import com.itau.geometria.formas.Forma;
import com.itau.geometria.formas.Interface;

public class App {
	public static void main(String[] args) {

		try {
			
//			EXERCÍCIO 1 - VALORES NO CÓDIGO			
// 
//			Forma circulo = new Circulo(2);
//			System.out.println("Área do circulo: " + circulo);
//
//			Forma retangulo = new Retangulo(2, 6);
//			System.out.println("Área do retangulo: " + retangulo);
//			
//			Forma triangulo = new Triangulo(2, 4, 4);
//			System.out.println("Área do triangulo: " + triangulo);
			
//			EXERCÍCIO 2 - VALORES PELA INTERFACE
			
			List<Integer> tamanhos = Interface.obterTamanhos();
			
			Forma f = FabricaFormas.construirForma(tamanhos);
			
			Interface.imprimirResultado(f);

		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

	}

}
