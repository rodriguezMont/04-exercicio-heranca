package com.itau.geometria.formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Interface {

	public static List<Integer> obterTamanhos() throws Exception {


		List<Integer> tamanhos = new ArrayList<Integer>();

		System.out.println("Olá! Insira até 3 tamanhos (t) para obter uma forma!");
		System.out.println("Opções: 1t - Circulo // 2t - Retangulo // 3t - Triangulo // 0 - Finalizar");

		Scanner sc = new Scanner(System.in);

		System.out.print("Insira um valor: ");
		Integer lerNumero = sc.nextInt();

		tamanhos.add(lerNumero);

		do {
			System.out.print("Insira outro valor: ");
			sc = new Scanner(System.in);
			lerNumero = sc.nextInt();
			
			if (lerNumero == 0)
				break;
			
			tamanhos.add(lerNumero);

		} while (lerNumero != 0);
		
		if(tamanhos.size() > 3)
			throw new Exception("Você inseriu mais de 3 tamanhos! Forma Inválida.");
		
		return tamanhos;
	}

	public static void imprimirResultado(Forma f) {
		
		System.out.println("Você escolheu um: " + f.getClass().getSimpleName());
		System.out.println("A area dele é: " + f);
		
	}
	
}
