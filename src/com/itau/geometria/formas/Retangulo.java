package com.itau.geometria.formas;

public class Retangulo extends Forma{
	
	private int base;
	private int altura;
	
	public Retangulo(int base, int altura) {
		this.base = base;
		this.altura = altura;
	}
	
	@Override
	public double calcularArea() {
		return this.base * this.altura;
	}

}
