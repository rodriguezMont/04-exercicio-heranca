package com.itau.geometria.formas;

import java.util.List;

public class FabricaFormas {

	public static Forma construirForma(List<Integer> tamanhos) throws Exception {

		Forma f = null;

		if (tamanhos.size() == 1) {

			f = new Circulo(tamanhos.get(0));

		} else if (tamanhos.size() == 2) {

			f = new Retangulo(tamanhos.get(0), tamanhos.get(1));

		} else if (tamanhos.size() == 3) {

			f = new Triangulo(tamanhos.get(0), tamanhos.get(1), tamanhos.get(2));

		}

		return f;

	}

}
