package com.itau.geometria.formas;

public class Triangulo extends Forma {

	private int ladoA;
	private int ladoB;
	private int ladoC;

	public Triangulo(int ladoA, int ladoB, int ladoC) throws Exception {

		if ((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA == true) {
			this.ladoA = ladoA;
			this.ladoB = ladoB;
			this.ladoC = ladoC;
		} else {
			throw new Exception("Triangulo inválido!");
		}

	}

	@Override
	public double calcularArea() {
		double s = (ladoA + ladoB + ladoC) / 2;
		Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
		return s;
	}

}
